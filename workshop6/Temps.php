<?php



if (($handle = fopen("temps.txt", "r")) !== FALSE) {
 
$temps =fgetcsv($handle);

sort($temps);
$cantidad=count($temps);
$sumatoria =array_sum($temps);
$promedio=$sumatoria/$cantidad;
$mayor = 0;
$menor = 0;
$lowTemps = array();
$highTemps = array();

$txtlow="";
for ($i = 0; $i < count($temps); $i++) {
   
    if ($mayor < $temps[$i]) {
        $mayor = $temps[$i];
        if (count($lowTemps) <= 4) {
            array_push($lowTemps, $temps[$i]);
            
        }
    }
}


$reversed = array_reverse($temps);

for ($i = 0; $i < count($reversed); $i++) {
    if ($mayor >= $reversed[$i]) {
        if (count($highTemps) <= 4) {
            array_push($highTemps, $reversed[$i]);
        }
    }
}
$lowString = "{$lowTemps[0]}, {$lowTemps[1]}, {$lowTemps[2]}, {$lowTemps[3]}, {$lowTemps[4]}";
$HighString = "{$highTemps[0]}, {$highTemps[1]}, {$highTemps[2]}, {$highTemps[3]}, {$highTemps[4]}";



echo 'Average Temperature is : '. $promedio.PHP_EOL;
echo 'List of 5 lowest temperatures (no duplicates) : '.$lowString .PHP_EOL;
echo 'List of 5 highest temperatures (no duplicate): : '.$HighString .PHP_EOL;

  fclose($handle);
}


