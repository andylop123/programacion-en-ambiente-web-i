<?php


if ($_POST) {
    include "Includ/connection.php";

    $Name = $_REQUEST['name'];
    $Description = $_REQUEST['description'];
    $datos = " INSERT INTO categoria (nombre,descripcion) VALUES('$Name','$Description')";
    $resultado = mysqli_query($connection, $datos);

    if (!$resultado > 0) {
        echo "<script>alert('Error al registrar')</script>";
    } else {
        echo "<script>alert('Registrado correctamente')</script>";
    }
    mysqli_close($connection);
}


 if (!empty($_REQUEST['id'])) {
    include "Includ/connection.php";
     $id = $_REQUEST['id'];
     $datos = "DELETE FROM categoria WHERE id = '$id'";
     $resultado = mysqli_query($connection, $datos);
     mysqli_close($connection);

    }

?>


<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css"
        integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Registro</title>
</head>

<body>

    <!-- Termina el header -->
    <div class="container pt-1">
        <div class="jumbotron bg-dark text-white ">
            <h4 class="display-2 text-center">Crud</h4>
        </div>
    </div>


    <div class="container ">


        <main class=" pt-4 pt-0 bg-light" style="margin-top: -2rem;">
            <form class="row g-4 needs-validation p-3" novalidate method="post">
                <div class="col-md-3">
                </div>
                <div class="col-md-3 text-center">
                    <label for="validationCustom02" class="form-label">Name</label>
                    <input type="text" class="form-control" required name="name">
                </div>
                <div class="col-md-3 text-center">
                    <label for="validationCustom02" class="form-label">Description</label>
                    <input type="text" class="form-control" required name="description">
                </div>
                <div class="col-md-3">
                </div>
                <div class="col-12 text-center">
                    <hr>
                    <button class="btn btn-dark" type="submit">Save</button>
                </div>
            </form>
        </main>
    </div>


    <div class="container">
        <table class="table table-dark table-striped">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Name</th>
                    <th scope="col">Description</th>
                    <th scope="col">Actions</th>

                </tr>
            </thead>
            <tbody>
                <?php

                include "Includ/connection.php";
                $datos = 'SELECT * FROM categoria';
                $resultado = mysqli_query($connection, $datos);
                while ($row = mysqli_fetch_assoc($resultado)) {

                ?>
                <tr>
                    <td><?php echo $row['id']; ?></td>
                    <td><?php echo $row['nombre']; ?></td>
                    <td><?php echo $row['descripcion']; ?></td>
                    <td>
                    <button class="btn btn btn-dark" type="submit" onclick="window.location.href='Edit.php?id=<?php echo $row['id'] ?>'">Edit</button><button
                            class="btn btn btn-dark" type="submit" onclick="window.location.href='workshop.php?id=<?php echo $row['id'] ?>'">Delete</button>
                    </td>
                </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>

</body>

</html>