<?php


if (!empty($_POST)) {
    include 'includs/Metodos.php';
    insert($_REQUEST);
}





?>


<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Registro</title>
</head>

<body>

    <!-- Termina el header -->
    <div class="container pt-1">
        <div class=" bg-info text-dark" style="height: 100px; ">
            <h1 class=" text-center">Registration Form</h4>
        </div>
    </div>


    <div class="container ">
        <main class="bg-white pt-4 pt-0" style="margin-top: -2rem;">
            <form class="text-center" id="form" method="POST">
                <div class="form-row justify-content-center">
                    <div class="form-group col-md-3">
                        <!-- <label for="inputEmail4">Email</label> -->
                        <input type="text" class="form-control" placeholder="Firts Name" required name="FirtsName">
                    </div>
                    <div class="form-group col-md-3">
                        <!-- <label for="inputPassword4">Password</label> -->
                        <input type="text" class="form-control" placeholder="Last Name" required name="LastName">
                    </div>

                </div>

                <div class="form-row d-flex flex-column align-items-center">

                    <div class="form-group col-md-3 ">
                        <!-- <label for="inputAddress">Address</label> -->
                        <input type="text" class="form-control" placeholder="ID Number" required name="ID">
                    </div>
                    <div class="form-group col-md-3">
                        <input type="text" class="form-control" placeholder="Email" aria-describedby="inputGroupPrepend" required name="Username">
                    </div>

                    <div class="form-group col-md-3">
                        <select class="form-control" name="Career">
                            <?php
                            include 'includs/Metodos.php';
                            $sql = " select * from carreras;";

                            foreach (datos($sql) as $career) : ?>
                                <option value=<?php echo $career[0] ?>><?php echo $career[1] ?></option>

                            <?php
                            endforeach;
                            ?>
                        </select>
                    </div>

                    <button type="submit" class="btn btn-secondary btn-sm m-1 mb-4" id="button-Register">Save</button>
                </div>








            </form>
        </main>
    </div>












</body>

</html>