CREATE TABLE matricula(
  id int AUTO_INCREMENT PRIMARY key,
  nombre text NOT null,
  apellido text NOT null,
  cedula text NOT null,
  email text NOT null,
  id_carrera int not null,
  FOREIGN KEY(id_carrera) REFERENCES carreras(id)
);
CREATE TABLE carreras(
  id int AUTO_INCREMENT PRIMARY key,
  nombre text NOT null
);


select * from matricula m INNER JOIN carreras c on m.id_carrera = c.id;