<?php
session_start();

$user = $_SESSION['user'];
if (!$user) {
  header('Location: Login.php');
} 
?>




<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Registro</title>
</head>

<body>

  <!-- Termina el header -->
  <div class="container pt-1 ">
    <div class=" bg-info text-dark" style="height: 100px; ">
      <h1 class=" text-center">Registration Form</h4>
      <ul class="nav justify-content-end m-1">
      <button type="submit" class="btn btn-secondary " id="button-Register" onclick="location.href='Logout.php'">Logout</button>
      </ul>

    </div>
  </div>



  <div class="container ">
  <h3 class="text-center pt-5">Welcome <?php echo $user[1]. ' '.$user[2]   ?></h3>
  </div>
</body>

</html>