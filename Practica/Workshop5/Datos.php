<?php

include_once 'Function.php';
$sql = 'select * from matricula m INNER JOIN carreras c on m.id_carrera = c.id;';
$datos = datos($sql);

if (isset($_GET['id'])) {
    //     $id=$_REQUEST['id'];
    //     include 'includs/connection.php';
    //     $datos = "DELETE FROM matricula WHERE id = '$id'";
    //     $resultado = mysqli_query($conection, $datos);
    //     mysqli_close($conection);

    $id =  $_GET['id'];
    delete($id);
    $sql = 'select * from matricula m INNER JOIN carreras c on m.id_carrera = c.id;';
    $datos = datos($sql);
}

session_start();

$user = $_SESSION['user'];
if (!$user) {
    header('Location: Login.php');
}

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Registro</title>
</head>

<body>

    <!-- Termina el header -->
    <div class="container pt-1">
        <div class=" bg-info text-dark" style="height: 100px; ">
            <h1 class=" text-center">Data Form</h4>
                <ul class="nav justify-content-end m-1">
                    <button type="submit" class="btn btn-secondary " id="button-Register" onclick="location.href='Logout.php'">Logout</button>
                </ul>
        </div>
    </div>


    <div class="container pt-2">

        <button type="submit" class="btn btn-secondary btn-sm m-1 mb-4" id="button-Register" onclick="window.location.href='index.php'">New</button>

    </div>


    <div class="container">
        <table class="table table-dark table-striped">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Id Number</th>
                    <th scope="col">First Name</th>
                    <th scope="col">Last Name</th>
                    <th scope="col">Email</th>
                    <th scope="col">Carreer</th>
                    <th scope="col">Actions</th>

                </tr>
            </thead>
            <tbody>
                <?php



                foreach ($datos as $career) { ?>


                    <tr>
                        <td><?php echo $career[0]; ?></td>
                        <td><?php echo $career[3] ?></td>
                        <td><?php echo $career[1]; ?></td>
                        <td><?php echo $career[2] ?></td>
                        <td><?php echo $career[4] ?></td>
                        <td><?php echo $career[7] ?></td>
                        <td><button class="btn btn-info" type="submit" onclick="window.location.href='Edit.php?id=<?php echo $career[0]; ?>'">Edit</button> <button class="btn btn-info" type="submit" onclick="window.location.href='Datos.php?id=<?php echo $career[0]; ?>'">Delete</button></td>

                    </tr>
                <?php } ?>







            </tbody>
        </table>
    </div>













</body>

</html>