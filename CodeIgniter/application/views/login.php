<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login</title>
</head>

<body>

    <!-- Termina el header -->
    <div class="container pt-2">
        <div class=" bg-info text-dark" style="height: 100px; ">
            <h1 class=" text-center p-3">Login Form</h4>
        </div>
    </div>

    <!-- tittle -->
    <div class="container">
        <main class="bg-white pt-4 pt-0" style="margin-top: -2rem;">
            <form class="text-center" method="POST" action="<?php echo site_url('user/authenticate') ?>">
                <div class="form-row d-flex flex-column align-items-center pt-5">

                    <div class="form-group col-md-3 ">
                        <input type="email" class="form-control " id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Correo electronico" name="email">
                    </div>
                    <div class="form-group col-md-3">
                        <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Contraseña" name="password">
                    </div>


                    <br>
                    <hr class=" bg-secondary w-50 ">
                    <label class="form-check-label text-secondary mb-3 ">Si no tiene una cuenta de usuario <a href="singup.php" style="color:#c95555; ">Regitrese aquí</a></label>


                   
                        <button type="submit " class="btn btn-secondary btn-sm m-1 mb-4 text-center" id="button-login">Ingresar</button>
                    
                    </div>
            </form>
        </main>


    </div>

</body>

</html>