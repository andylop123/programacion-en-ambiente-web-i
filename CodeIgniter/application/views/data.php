<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Registro</title>
</head>

<body>

    <!-- Termina el header -->
    <div class="container pt-1">
        <div class=" bg-info text-dark" style="height: 100px; ">
            <h1 class=" text-center">Data Form</h4>
                <!-- <ul class="nav justify-content-end m-1">
                    <button type="submit" class="btn btn-secondary " id="button-Register" onclick="location.href='Logout.php'">Logout</button>
                </ul> -->
        </div>
    </div>


    <div class="container pt-2">

        <a class="btn btn-secondary btn-sm m-1 mb-4" href="<?php echo site_url('user/add'); ?>">New</a>
        <!-- <button type="submit" class="btn btn-secondary btn-sm m-1 mb-4" id="button-Register" onclick="window.location.href='index.php'">New</button> -->

    </div>


    <div class="container">
        <table class="table table-dark table-striped">
            <thead>
                <tr>
                    <th scope="col">First Name</th>
                    <th scope="col">Last Name</th>
                    <th scope="col">Type</th>
                    <th scope="col">Actions</th>

                </tr>
            </thead>
            <tbody>
                <?php



                foreach ($users  as $user) { ?>


                    <tr>
                        <td><?php echo $user->nombre; ?></td>
                        <td><?php echo $user->apellido ?></td>
                        <td><?php echo $user->tipo ?></td>
                        <td>
                            <a class="btn btn-info" href="<?php  echo site_url('user/edit?id='.$user->id); ?>">Edit</a>
                            <a class="btn btn-info"  href="<?php  echo site_url('user/delete?id='.$user->id); ?>">Delete</a>
                        </td>


                    </tr>
                <?php } ?>







            </tbody>
        </table>
    </div>













</body>

</html>