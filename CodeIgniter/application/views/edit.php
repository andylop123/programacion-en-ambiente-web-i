<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Registro</title>
</head>

<body>

    <!-- Termina el header -->
    <div class="container pt-2">
        <div class=" bg-info text-dark" style="height: 100px; ">
            <h1 class=" text-center p-3">Singup</h4>
        </div>
    </div>


    <div class="container ">
        <main class="bg-white pt-4 pt-0" style="margin-top: -2rem;">
            <form class="text-center" id="form" method="POST" action="<?php echo site_url('user/editUser') ?>">
                <div class="form-row justify-content-center">
                    <div class="form-group col-md-3">
                        <!-- <label for="inputEmail4">Email</label> -->
                        <input type="text" class="form-control" placeholder="Firts Name" required name="nombre" value="<?php echo $user->nombre ?>">
                    </div>
                    <div class="form-group col-md-3">
                        <!-- <label for="inputPassword4">Password</label> -->
                        <input type="text" class="form-control" placeholder="Last Name" required name="apellido" value="<?php echo $user->apellido ?>">
                    </div>

                </div>

                <div class="form-row d-flex flex-column align-items-center">

                    <div class="form-group col-md-3 ">
                        <!-- <label for="inputAddress">Address</label> -->
                        <input type="password" class="form-control" placeholder="Password" required name="contrasena" value="<?php echo $user->contrasena ?>">
                    </div>
                    <div class="form-group col-md-3">
                        <input type="text" class="form-control" placeholder="Email" aria-describedby="inputGroupPrepend" required name="email" value="<?php echo $user->email ?>">
                    </div>

                    <div class="form-group col-md-3">
                        <select class="form-control" name="tipo">
                            <option value="<?php echo $user->tipo?>" selected disabled hidden><?php echo $user->tipo?></option>
                            <option value="Estudiante">Estudiante</option>
                            <option value="Administrador">Administrador</option>
                        </select>
                    </div>

                    <button type="submit" class="btn btn-secondary btn-sm m-1 mb-4" id="button-Register">Sing Up</button>
                </div>
            </form>
        </main>
    </div>


</body>

</html>