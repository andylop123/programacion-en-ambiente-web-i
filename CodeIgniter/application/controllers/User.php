<?php
defined('BASEPATH') or exit('No direct script access allowed');

class User extends CI_Controller
{

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/login
	 *	- or -
	 * 		http://example.com/index.php/login/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function login()
	{
		$this->load->view('login');
	}


	public function add()
	{
		$this->load->view('prueba');
	}



	public function edit()
	{
		$this->load->model('User_model');
		$id = $this->input->get('id');
		$data['user'] = $this->User_model->userById($id);

		$this->load->view('edit',$data);
	}


public function editUser(){

	$this->load->model('User_model');
	$data = $this->input->post();
	$result = $this->User_model->editUser($data);

	if ($result) {
		redirect('user/data');
	} else {

		// send errors
		$this->session->set_flashdata('msg', 'There was an error');
		redirect('user/edit');
	}




}

	public function delete()
	{
		$this->load->model('User_model');
		$id = $this->input->get();
		$this->User_model->delete($id);
		redirect('user/data');
	}


	public function data()
	{
		$this->load->model('User_model');
		$data['users'] = $this->User_model->all();

		$this->load->view('data', $data);
	}

	public function authenticate()
	{
		$username = $this->input->post('email');
		$password = $this->input->post('password');
		$this->load->model('User_model');
		$user = $this->User_model->authenticate($username, $password);
		var_dump($user);
		if ($user) {
			redirect('user/data');
		} else {
			redirect('user/login');
		}
	}


	public function inserUser()
	{
		$this->load->model('User_model');
		$data = $this->input->post();
		$result = $this->User_model->insert($data);

		if ($result) {
			redirect('user/data');
		} else {

			// send errors
			$this->session->set_flashdata('msg', 'There was an error');
			redirect('user/singup');
		}
	}
}
