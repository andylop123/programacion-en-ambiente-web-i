<?php
require_once('DbConnection.php');

class Student {

  public $id;
  public $Name;
  public $lastName;
  public $cedula;
  public $age;

  function __construct( $id,$Name,$lastName, $cedula, $age){
    $this->id = $id;
    $this->Name = $Name;
    $this->lastName = $lastName;
    $this->cedula = $cedula;
    $this->age = $age;
  }


  /**
   * Get the students from the database
   */
  static function getStudents($limit = 100) {
    $mysqlConnection = new DbConnection();
    $conection=  $mysqlConnection->getMySQLConnection();
    $datos ="SELECT * from student LIMIT $limit";
    $resultado = mysqli_query($conection, $datos);
    $data = mysqli_fetch_all($resultado);
    mysqli_close($conection);
  return $data;
  }





  

  /**
   * Save the current student into the database
   */
  function save(){

  }

  /**
   * Returns a CSV representation of the student
   */
  function toCsv(){
    return "{$this->id}, {$this->Name}, {$this->lastName}, {$this->cedula}, {$this->age}";
  }

} 