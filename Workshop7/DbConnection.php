<?php
class DbConnection
{
  private $host = 'localhost:3306';
  private $user = 'root';
  private $password = '';
  private $databaseName = 'workshop';




  function getMySQLConnection()
  {
    $conection = mysqli_connect($this->host, $this->user, $this->password, $this->databaseName);
    if ($conection->connect_errno) {
      echo "Error: $conection->connect_error\n";
      exit;
    }
    return $conection;
  }
}
